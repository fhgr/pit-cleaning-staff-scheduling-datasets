Cleaning Staff Scheduling Datasets for Constraint-Based Property Management in Tourism (CSSD-PIT)
================================================================================================

[![License: CC BY-SA 4.0](https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg)](https://creativecommons.org/licenses/by-sa/4.0/)

The following datasets were developed for an applied research project for constraint-based cleaning staff scheduling in tourism. The (real world data) was collected in 2020 from a holiday property management system by the project team. It consists of three different datasets.

**[Dataset SMALL](data/pit_SMALL.xlsx)**: Consists of 1 depot, 3 cleaning teams and 9 properties of different sizes and with different time windows.

**[Dataset MEDIUM](data/pit_MEDIUM.xlsx)**: Consists of 1 depot, 20 cleaning teams and 77 properties of different sizes and with different time windows.

**[Dataset LARGE](data/pit_LARGE.xlsx)**: Consists of 2 depots, 45 cleaning teams and 107 properties of different sizes.

The datasets can be used to solve a Vehicle Routing Problem with Time Windows (VRPTW).

Getting Started
---------------
A good starting point when thinking about VRPTW is the [OptaPlanner](https://www.optaplanner.org/) constraint solver.

Participating institutions
--------------------------
* [Institute for Photonics and ICT (IPI), University of Applies Sciences of Grisons](https://www.fhgr.ch/en/uas-grisons/applied-future-technologies/institute-for-photonics-and-ict-ipi/)
* [Institute for Tourism and Leisure (ITF), University of Applies Sciences of Grisons](https://www.fhgr.ch/en/uas-grisons/alpine-region-development/institute-for-tourism-and-leisure-itf/)
* [LAAX Homes](https://www.laaxhomes.com/)
* [Weisse Arena Gruppe](https://weissearena.com/)

How to cite
-----------
Capol, C., Balestra, S., Staudt, Y., & Jacobson, C. (2022). Scheduling Application for Cleaning Staff in Tourism: Constraint-Based Solution in the Field of Property Management in Tourism Regions. Working Paper of the University of Applied Sciences of the Grisons.
